# Ce fichier contient les fonctions que j'ai utilisé pour extraire les données du Benin
#chargement des packages
if(!require("pacman")) install.packages("pacman")
pacman::p_load("dplyr",
               "tidyverse")

# Etape 1 : extraction des données du Niger
##### fonction pour ajouter la colonne hhid (identifiant menage unique pour chaque pays) #####
add_hhid = function(data, code_pays){
  data = data %>% mutate(hhid = paste0(code_pays, 
                                       data$grappe, 
                                       "00", data$menage))%>% select(hhid, 
                                                                     grappe, menage, everything()) 
  return(data)
}

# base de données infos de base sur les ménages
data_00_men_ben = add_hhid(data = vars_me_id_ben, code_pays = "ben")
# base de données sur l'éducation
data_02_men_ben = add_hhid(data = vars_me_s2_ben, code_pays = "ben")
# base de données age, statut martial
data_01_men_ben = add_hhid(data = vars_me_s1_ben, code_pays = "ben")
# base de données sur l'epargne 
data_06_men_ben = add_hhid(data = vars_me_s6_ben, code_pays = "ben")
# base de données logement (menage connecté à un réseau électrique)
data_11_men_ben = add_hhid(data = vars_me_s11_ben, code_pays = "ben")

# section 4 emploi
data_04_men_ben = add_hhid(vars_me_s4_ben, code_pays = "ben")
# base de données sur les entreprises non agricoles
data_10_1_men_ben = add_hhid(vars_me_s10_ben, code_pays = "ben")

# base de données sur les actifs
data_12_men_ben = add_hhid(vars_me_s12_ben, code_pays = "ben")
# base de données sur les chocs
data_14_men_ben = add_hhid(vars_me_s14_ben, code_pays = "ben")
# base de données sur l'élevage 
data_17_men_ben = add_hhid(data = vars_me_s17_ben, code_pays = "ben")
# base de données sur la pauvreté
data_20_men_ben = add_hhid(data = vars_me_s20_ben, code_pays = "ben")
# base de données sur l'agriculture
data_16a_men_ben = add_hhid(data = vars_me_s16a_ben, code_pays = "ben") %>% distinct(hhid, .keep_all = TRUE)

########## fonctions individus ##########
#### fonction pour détermiben le genre de l'individu ####
attribgender = function(individ, id){
  gender_info = data_01_men_ben %>%
    filter(hhid == id, id_code == individ) %>%
    pull(sexe)
  
  if (length(gender_info) > 0) {
    return(gender_info == 1)  # Retourne TRUE si (1:homme)
  } else {
    return(NA)  
  }
}

### fonction pour déterminer le genre d'un groupe de proprios ###
get_owner_gender <- function(owner_ids, hhid) {
  if (is.null(owner_ids) || length(owner_ids) == 0) {
    return(NA)
  }
  
  # regarder le genre de chaque propriétaire, TRUE : homme, FALSE : femme
  genders <- map(owner_ids, ~attribgender(.x, hhid))
  
  # calcul du nombre d'hommes et de femmes
  num_men <- sum(map_lgl(genders, ~.x == TRUE), na.rm = TRUE)
  num_women <- sum(map_lgl(genders, ~.x == FALSE), na.rm = TRUE)
  
  # Retourner la catégorie selon les cas (5 alternatives:  
  #"unique_woman" (un seul propriétaire femme), "unique_man", "several_woman", "several_man", 
  #"several_man_woman" (plusieurs propriétaires incluant des hommes et des femmes))
  case_when(
    num_men == 1 & num_women == 0 ~ "unique_man",        # Un seul homme propriétaire
    num_men == 0 & num_women == 1 ~ "unique_woman",      # Une seule femme propriétaire
    num_men > 1 & num_women == 0 ~ "several_man",        # Plusieurs hommes
    num_women > 1 & num_men == 0 ~ "several_woman",      # Plusieurs femmes
    num_men >= 1 & num_women >= 1 ~ "several_man_woman", # Hommes et femmes
    TRUE ~ NA
  )
}
#### fonction pour déterminer si le propriétaire pratique la religion dominante ####
attribreligion = function(individ, id){
  religion_info = data_01_men_ben %>%
    filter(hhid == id, id_code == individ) %>%
    pull(religion)
  
  if (length(religion_info) > 0) {
    return(religion_info == 2)  # Retourne TRUE si (2:chretien)
  } else {
    return(NA)  
  }
}

#### fonction qui détermine si au moins un des proprios pratique la religion dominante du Niger (islam) ####
get_owner_religion <- function(owner_ids, hhid) {
  
  # Vérifiez si owner_ids est NULL, vide, ou integer(0), et traitez-le comme NA
  if (is.null(owner_ids) || length(owner_ids) == 0) {
    return(NA)
  }
  
  # Vérifiez la religion de chaque propriétaire
  religions <- map(owner_ids, ~ attribreligion(.x, hhid))
  
  # Si toutes les valeurs sont NA, retourner NA
  if (all(is.na(religions))) {
    return(NA)
  }
  
  # Sinon, vérifier si au moins un propriétaire a religion == 2
  any_religion <- any(map_lgl(religions, ~ .x == TRUE), na.rm = TRUE)
  
  return(any_religion)
}



#### fonction pour vérifier si un individu est allé à l'école ####
attribschool = function(individ, id){
  # id : identifiant menage
  # individ : identifiant de l'individu
  school = with(data_02_men_ben, data_02_men_ben[hhid == id & id_code == individ, "a_etudie"])
  return(ifelse(length(school)>0, school==1, NA))
}
attribschool(individ = 4, id = "ben1001") # ok c'est false
attribschool(individ = 2, id = "ben1002") # ok c'est true

#filtrer les données pour un ménage et un individu donné 
#extraire la colonne de l'éducation (niveau actuel), et vérifier si la valeur indique que l'individu est allé à l'école ou pas

#### Fonction pour détermiben si un individu est marié #####
attribmarried = function(individ, id){
  married = with(data_01_men_ben, data_01_men_ben[hhid == id & id_code == individ, "statut_martial"])
  return(ifelse(length(married)>0, married%in%c(2,3), NA)) # 2 :marié monogame, 3 :marié polygame
}
# menage = 1, ind = 4, NA, on retourne FALSE
# menage = 1, ind = 1, 2, on retourne TRUE
attribmarried(individ = 4, id = "ben1001") # ok
attribmarried(individ = 2, id = "ben1001") # ok



###### fonction qui détermine si un individu possède un compte un banque ####
attribbank = function(individ, id) {
  # infos sur les comptes pour l'individu
  bank_info = data_06_men_ben %>%
    filter(hhid == id, id_code == individ) %>%
    select(starts_with("s06q01__")) %>%
    na.omit()  # on supprime les lignes entièrement composées de NA
  
  # si aucune ligne n'est disponible après suppression des NA on retourne na
  if(nrow(bank_info) == 0) {
    return(NA)
  } else {
    # on vérifie si l'individu possède au moins un type de compte
    has_account = any(bank_info == 1, na.rm = TRUE)
    return(has_account)
  }
}

attribbank(1, "ben2004") # c'est bien TRUE
attribbank(1, "ben2006") # c'est bien FALSE
attribbank(1, "ben29004") # c'est bien NA

####### fonction pour vérifier si un individu a une activité économique ######
attribjob <- function(individ, id) {
  # Sélectionben les données pour l'individu spécifique dans le ménage spécifique
  job_data <- data_04_men_ben %>%
    filter(hhid == id, id_code == individ) %>%
    select(travail_champs, travail_remun_com, travail_salarie, travail_non_exerce)
  
  # Vérifier si toutes les réponses sont NA
  if (all(is.na(job_data))) {
    return(NA)  # Retourne NA si aucune information n'est disponible
  } else {
    # Détermine si l'individu a eu une activité économique
    has_activity <- any(job_data == 1, na.rm = TRUE)
    return(has_activity)
  }
}

attribjob(1, 'ben1001') # ok c'est bien NA
attribjob(1, "ben1007") # ok c'est buen TRUE
attribjob(1, "ben3009") # ok c'est bien false youpiii

# Fonction pour vérifier si un individu a accès à Internet de n'importe quelle manière spécifiée
attribinternet = function(individ, id) {
  # Extraire les données pour l'individu spécifique dans le ménage spécifique
  internet_access = data_01_men_ben %>% 
    filter(hhid == id, id_code == individ) %>%
    select(internet_tel:internet_ecole)
  
  if(nrow(internet_access) > 0) {
    # Vérifie si au moins une des manières d'accès à internet est vraie (1=oui, 0=non)
    return(any(internet_access == 1, na.rm = TRUE))
  } else {
    # Retourne NA s'il a rien trouvé pour cet individu
    return(NA)
  }
}
# test
attribinternet(1, "ben1001") # c'est bien false
attribinternet(2, "ben1002") # c'est bien true

# fonction pour savoir si un individu au sein d'un menage a un telephone
attribphone = function(individ, id){
  phone = with(data_01_men_ben, data_01_men_ben[hhid == id & id_code == individ, "a_telephone"])
  return(ifelse(length(phone) > 0, phone == 1, NA))
}

# test
attribphone(1, "ben1001") # ok c'est false
attribphone(2, "ben1002") # ok c'est true

#### fonction qui détermine si un individu possède un compte crédit ou pas ####
attribcredit = function(individ, id){
  credit = data_06_men_ben %>% filter(hhid == id, id_code == individ) %>% select(s06q10) %>% pull(s06q10)
  if(length(credit) == 0){
    return(NA) # si pas de ligne NA
  } else if(is.na(credit)){
    # si on a pas de donnée on retourne FALSE
    return(FALSE)
  } else{
    return(TRUE) # il y a une valeur qui indique que l'individu a eu un crédit
  }
}

# verification :
# selection des id des individus qui ont eu un credit
data_06_men_ben %>% filter(s06q10 > 1 & !is.na(s06q10)) %>% select(hhid, id_code, s06q10)
attribcredit(individ = 1, id = "ben1006") # yes c'est bien TRUE
attribcredit(individ = 1, id = "ben2007")

# selection des id des individus qui n'ont pas eu de credit
data_06_men_ben %>% filter(is.na(s06q10)) %>% select(hhid, id_code, s06q10)
attribcredit(individ = 3, id = "ben1001") # yes c'est bien FALSE car NA
attribcredit(individ = 5, id = "ben6005")

# individu qui n'existe pas
sort(unique(data_06_men_ben$id_code)) # individ 35 et 36 par ex
data_06_men_ben %>% filter(hhid == "ben1001", id_code == 35) # il n'existe pas
attribcredit(individ = 35, id = "ben1001") # super c'est bien NA
attribcredit(individ = 36, id = "ben1001") # super c'est bien NA


########## fonctions ménage ##########
####### fonction pour vérifier si le ménage possède un mouton ou une chèvre #####
has_specie <- function(id, species_codes) {
  return(with(data_17_men_ben, any(hhid == id & code_anim %in% c(species_codes) & menage_possede == 1)))
}

has_specie(id = "ben1001", species_codes = 2)
has_specie(id = "ben1001", species_codes = 3)


###### fonction qui détermine si le menage est connecté à un réseau de tel ######
attrib_reseaum = function(id){
  phone = with(data_11_men_ben, data_11_men_ben[hhid == id, "household_reseau_tel"])
  return(ifelse(length(phone)>0, phone == 1, NA))
}
# verification
data_11_men_ben %>% filter(household_reseau_tel == 1) %>% select(hhid, household_reseau_tel) #ceux qui captent le reseau mobile, y en a que 2!!

attrib_reseaum(id = "ben224006") #ok!
attrib_reseaum(id = "ben1001") #c'est bien false, ok!

###### fonction qui détermine si le menage est connecté à un réseau électrique (s11q43) ######
attrib_reseau_elect = function(id){
  reseau_elct = with(data_11_men_ben, data_11_men_ben[hhid == id, "household_reseau_elect"])
  return(ifelse(length(reseau_elct)>0, reseau_elct %in% c(1, 2, 3), NA)) # 1 : connecté, 2 : connecté chez un voisin, 3 : directement au poteau
}
data_11_men_ben %>% filter(household_reseau_elect %in% c(1, 2, 3)) %>% select(hhid) #ceux qui sont connectés
attrib_reseau_elect(id = "ben2005") # ok
data_11_men_ben %>% filter(household_reseau_elect == 4) %>% select(hhid) # ceux qui sont pas connectés
attrib_reseau_elect(id = "ben1040012") # ok!

#### fonction qui determine si le menage possede une entreprise non agricole

attrib_entreprise_nonag = function(id){
  entreprise_non_ag = with(data_10_1_men_ben, data_10_1_men_ben[hhid == id, "entreprise_nonag_pres"])
  return(ifelse(length(entreprise_non_ag) > 0, entreprise_non_ag == 1, NA))
}
# vérification
data_10_1_men_ben %>% filter(entreprise_nonag_pres == 1) %>% select(hhid) #ceux qui posèdent une entreprise non agricole
attrib_entreprise_nonag(id = "ben1003") # ok c'est bien true
data_10_1_men_ben %>% filter(entreprise_nonag_pres == 0) %>% select(hhid)
attrib_entreprise_nonag(id = "ben1001") # ok c'est bien false

###### fonction pour verifier si le menage possede l'un des articles (tv, etc) #####
possede_article <- function(menage, id_articles) {
  # Utiliser ifelse pour vérifier l'existence de l'identifiant du ménage
  possede <- ifelse(menage %in% data_12_men_ben$hhid,
                    nrow(data_12_men_ben %>% filter(hhid == menage,
                                                    id_article %in% id_articles,
                                                    possede_article == 1)) > 0, NA)
  
  return(possede)
}

##### fonction pour calculer le capital immobilier du ménage (wealth) #####
wealth_men = function(id){
  # Filtrer les données pour un ménage spécifique et seulement pour les articles immobiliers
  wealth_data = data_12_men_ben %>%
    filter(hhid == id, possede_article == 1, id_article %in% c(44, 45)) %>%
    select(val_fcfa, nb_article)  # Sélectionben les colonnes nécessaires
  
  # Calculer la valeur totale du capital immobilier
  if (nrow(wealth_data) > 0) {
    # Multiplier la valeur de chaque article par son nombre puis sommer les résultats
    rev_men = sum(wealth_data$val_fcfa * wealth_data$nb_article, na.rm = TRUE)
  } else {
    # Retourben 0 si aucune donnée correspondante n'est trouvée
    rev_men = 0
  }
  
  return(rev_men)
}

# verification 
data_12_men_ben %>% filter(id_article %in% c(44, 45), nb_article > 1) %>% select(hhid, id_article ,val_fcfa, nb_article)

wealth_men(id = "ben1001") # ok 50000
wealth_men(id = "ben1004") # ok 40000

wealth_men("ben6007") # ok c'est bien 600000*2 = 1200000

# le ménage le plus riche :
data_12_men_ben %>% filter(hhid == "ben4480012") %>% select(hhid, val_fcfa, nb_article, id_article)
# 1000000*14 + 20000000*9 = 1.94e+08
wealth_men("ben4480012") # ok 194000000

data_12_men_ben %>% filter(hhid == "ben2007", possede_article == 1) %>% select(val_fcfa, nb_article, id_article)
wealth_men("ben2007") # 580000 ok 

###### fonction pour calculer la valeur totale de l'achat pour un ménage donné (en excluant les biens immobiliers) #######

total_achat <- function(id) {
  filtered_data <- data_12_men_ben %>%
    filter(hhid == id, possede_article == 1, !id_article %in% c(44, 45))
  
  total_purchase_value <- filtered_data %>%
    mutate(total_val_achat = nb_article * val_fcfa) %>%
    summarise(total_val_achat = sum(total_val_achat, na.rm = TRUE)) %>%
    pull(total_val_achat)
  
  if (nrow(filtered_data) == 0) {
    return(0)  # Retourne 0 si le ménage ne possede pas d'articles
  }
  
  return(total_purchase_value)
}

# test
data_12_men_ben %>% filter(hhid == "ben1001")
total_achat("ben1001") # ok c'est 0

data_12_men_ben %>% filter(possede_article == 1, !id_article %in% c(44, 45)) %>% select(hhid, id_article, val_fcfa, nb_article) 
total_achat("ben1002") # 20000 * 2
total_achat("ben2001")

### idem pour la valeur totale de revente des articles par le ménage ####
total_revente = function(id){
  data_revente = data_12_men_ben %>% filter(hhid == id, possede_article == 1, !id_article %in% c(44, 45))
  total_revente = data_revente %>%
    mutate(total_prix_revente = nb_article * prix_revente) %>%
    summarise(total_prix_revente = sum(total_prix_revente, na.rm = TRUE)) %>%
    pull(total_prix_revente)
  
  if(nrow(data_revente) == 0){
    return(0)
  } 
  return(total_revente)
}

data_12_men_ben %>% filter(possede_article == 1, !id_article %in% c(44, 45)) %>% select(hhid, id_article, prix_revente, nb_article)
total_revente("ben1001") # ok c'est bien 0
total_revente("ben1002") # 15000 * 2 ok


# #### fonction pour calculer le nombre de moutons et chèvres ####
count_animals <- function(menage, spiece_code) {
  animal_count <- data_17_men_ben %>%
    filter(hhid == menage, code_anim == spiece_code) %>%
    summarise(total = sum(taille_troup, na.rm = TRUE)) %>%
    pull(total)
  return(animal_count)
}

count_animals(menage = "ben1001", 2) # il n'a pas de moutons
count_animals(menage = "ben1001", 3)

