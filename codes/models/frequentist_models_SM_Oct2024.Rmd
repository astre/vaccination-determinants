---
title: "Modèles fréquentistes - Projet vaccination"
author: "Sarah Matoub"
date: "2024-10-21"
output:
  pdf_document:
    latex_engine: pdflatex
    toc: true
    toc_depth: 4
    number_sections: true
    keep_tex: true
    template: null
    fig_caption: true
header-includes: 
  - \usepackage[french]{babel}
  - \usepackage{array}
geometry: left=1.5cm,right=1.5cm,top=1.5cm,bottom=1.5cm
fontsize: 11pt
mainfont: Arial
---

```{r setup, include=FALSE, cache=FALSE}
#knitr::opts_knit$set(root.dir = "Vaccination determinants (gitlab)")
knitr::opts_chunk$set(
  fig.width = 13, fig.height = 6,
  echo = FALSE,
  cache = FALSE,
  eval = TRUE
)

if(!require(pacman)){
  install.packages("pacman")
}
pacman::p_load("here",
               "factoextra",
               "FactoMineR",
               "Factoshiny",
               "tinytex", 
               "ggplot2",
               "dplyr",
               "tidyr",
               "purrr", 
               "lme4",
               "glmmLasso",
               "DHARMa",
               "cv.glmmLasso",
               "MASS"
               )
#load("D:/Mes Donnees/Vaccination determinants (gitlab)/codes/models/model.RData")
```

This document presents scripts for analyzing LSMS data from surveys conducted in seven West African countries: Niger, Mali, Senegal, Burkina Faso (BFA), Côte d'Ivoire (CIV), Togo, and Benin. It also includes frequentist models utilized in this study, such as Factorial Analysis of Mixed Data (FAMD), Multiple Factor Analysis (MFA), Principal Component Regression (PCR), and Least Absolute Shrinkage and Selection Operator (LASSO).

We have implemented several changes based on the results presented in the document `synthese_des_nouveaux_resultats`, derived from the script `new_data_analysis_sarah.Rmd`. These changes include:

- Adding a new variable, `owner_gender`, which includes five categories:
"unique_woman" (one female owner), "unique_man" (one male owner),"several_woman" (several female owners)
"several_man" (several male owners), "several_man_woman" (several owners including both men and women).

This is due to the fact that the variables `owner_man` and `owner_woman` positively influenced the probability of vaccination, although we cannot determine the exact nature of this influence.

This allowed us to identify an error in the coding of certain variables. For instance, some households had no owner (all owner identifiers were NA), yet the values for owner_woman and owner_man were incorrectly set to FALSE instead of NA. We corrected this issue, resulting in 15,545 observations instead of 16,910.

- We standardized all quantitative variables because those related to household wealth had coefficient estimates close to zero due to their high values.

- We removed the variable `moy_transp_pr_freq` from our model because we suspect that it was correlated with the variable `moy_transp_pr`, as they significantly inflated the standard error and resulted in p-values of 1 in the frequentist model.

# FAMD

```{r data-pretraitement}
std_data_all_new = dplyr::select(std_data_all, -vaccine, -code_anim, -owner_woman, -owner_man, -moy_transp_pr_freq, -grappe)
rownames(std_data_all_new) = std_data_all$hhid
# Fonction pour renommer les modalités des variables qualitatives
rename_levels <- function(df) {
  df %>%
    mutate(across(where(is.factor), ~ {
      var_name <- cur_column()
      levels(.) <- paste0(var_name, "_", levels(.))
      .
    }))
}
df_FAMD = rename_levels(std_data_all_new[,-1])
dim(df_FAMD)
colnames(df_FAMD)
```


```{r FAMD}
res.FAMD = FAMD(df_FAMD,
                graph = FALSE,
                ncp = 200)
#get_eigenvalue(res.FAMD) # for visualisation 3PC, for clustering 11PC

```

```{r eigenvalues}
fviz_screeplot(res.FAMD, addlabels = TRUE, barfill="#006666", barcolor = "#006666", main="Distribution de l'inertie")

```


```{r quanti-var}
# plan (1,2)
fviz_famd_var(
  res.FAMD,
  "quanti.var",
  col.var = "contrib",
  gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
  #select.var = list(cos2 = 7),
  repel = TRUE,
  axes = c(1, 2)
)
# plan (1,3)
fviz_famd_var(
  res.FAMD,
  "quanti.var",
  col.var = "contrib",
  gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
  #select.var = list(cos2 = 7),
  repel = TRUE,
  axes = c(1, 3)
)
```


```{r quali-var}
# plan (1,2)
fviz_famd_var(
  res.FAMD,
  "quali.var",
  col.var = "contrib",
  gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
  select.var = list(contrib = 30),
  repel = TRUE,
  axes = c(1, 2)
)
# plan(1,3)
fviz_famd_var(
  res.FAMD,
  "quali.var",
  col.var = "contrib",
  gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
  select.var = list(contrib = 30),
  repel = TRUE,
  axes = c(1, 3)
)
```

```{r individuals}
vaccine = as.factor(std_data_all$vaccine)
fviz_mfa_ind(res.FAMD, 
             habillage = as.factor(vaccine), 
             labels = FALSE,
             legend.title = "Vaccine",
             axes = c(1, 2)
             ) 
```

# MFA

```{r MFA}
newDF <- std_data_all[, -1][,c("distance_ville_pr","head_sexe","head_martial_status","head_religion","head_economic_activity","head_bank_account","head_credit","owner_head","owner_gender","owner_married","owner_bank_account","owner_economic_activity","owner_credit","voie_acces_pr","transp_dispo","transp_moto","transp_train","transp_voiture","transp_pirogue","moy_transp_pr","presence_cs","reseau_elect","reseau_mobile","service_hospital","service_other_health","service_medical_clinic","service_bank","service_permanent_market","service_periodic_market","service_agricultural_center","service_feed_bank","region_nom","country","reseau_elect","household_reseau_elect","service_hospital","service_other_health","service_medical_clinic","service_bank","service_permanent_market","service_periodic_market","service_agricultural_center","service_feed_bank","reseau_mobile","household_reseau_tel","household_tv","household_radio","presence_cs","head_phone","head_internet","head_education","owner_phone","owner_internet","owner_school","household_val_achat","household_prix_revente","household_wealth_immobilier","household_cultive_terre","entreprise_nonag_pres","head_economic_activity","head_bank_account","head_credit","owner_bank_account","owner_economic_activity","owner_credit","household_percep_rev","household_percep_rev_cl","household_percep_rev_voisins","household_percep_rev_capitale","household_diff_ecl","household_diff_loyer","household_diff_maladie","household_diff_transport","household_diff_sco","owner_num","owner_number_sold","owner_number_purchased","owner_value_purchased","owner_dairy_revenue","owner_healthcare_expenditure","household_percep_rev_mttmin","household_cultive_terre","owner_decision_sale","owner_milk_production","owner_meat_production","owner_sale_part_meat","owner_sale_part_milk","owner_purchase_feed","owner_watering_payment","owner_deworming","owner_healthcare","household_choc","choc_str_epargne","choc_str_aide_parents_amis","choc_str_aide_gouv","choc_str_aide_rel_ONG","choc_str_marier_enfants","choc_str_change_conso","choc_str_achat_aliments_moins_chers","choc_str_membres_actifs_emplois_supp","choc_str_membres_adultes_emplois","choc_str_enfants_travailler","choc_str_enfants_descolarises","choc_str_migration","choc_str_reduction_depenses_sante_educ","choc_str_obtention_credit","choc_str_vente_actifs_agricoles","choc_str_vente_biens_durables","choc_str_vente_terrain_immeubles","choc_str_location_terres","choc_str_vente_stock_vivres","choc_str_activites_peche","choc_str_vente_betail","choc_str_confiage_enfants","choc_str_activites_spirituelles","choc_str_culture_contre_saison","choc_str_autre_strategie","choc_str_aucune_strategie")]
new_mfa <-
  MFA(
    newDF,
    group = c(1, 30, 12, 11, 3, 17, 7, 37),
    type = c("c", "n", "n", "n", "c", "n", "c", "n"),
    name.group = c(
      "Accessibility_quanti",
      "Accessibility_quali",
      "Availability",
      "Awareness",
      "Affordability_quanti",
      "Affordability_quali",
      "Acceptability_quanti",
      "Acceptability_quali"
    ),
    ncp = 50,
    graph = FALSE
  )
```

## Eigenvalues

```{r eigenvalues-mfa}
head(get_eigenvalue(new_mfa), 10)
```
For PCR, we will consider the 7 first principal components since their eigenvalues are < 1.

```{r}
fviz_screeplot(
  new_mfa,
  addlabels = TRUE,
  barfill = "#006666",
  barcolor = "#006666",
  main = "Distribution de l'inertie"
)
```

## Quantitative variables 

```{r quanti-var}
fviz_mfa_var(new_mfa, "quanti.var", palette = c("#005580", "#b300b3", "#b33c00"),
             repel = TRUE, 
             title = "Cercle des corrélations sur le plan (1,2)",
             axes = c(1, 2),
             #geom = c("point", "text"), 
             legend = "bottom")
fviz_mfa_var(new_mfa, "quanti.var", palette = c("#005580", "#b300b3", "#b33c00"),
             repel = TRUE, 
             title = "Cercle des corrélations sur le plan (1,3)",
             axes = c(1, 3),
             #geom = c("point", "text"), 
             legend = "bottom")
# plan 1,4
fviz_mfa_var(new_mfa, "quanti.var", palette = c("#005580", "#b300b3", "#b33c00"),
             repel = TRUE, 
             title = "Cercle des corrélations sur le plan (1,4)",
             axes = c(1, 4),
             #geom = c("point", "text"), 
             legend = "bottom")
# plan (1,5)
fviz_mfa_var(new_mfa, "quanti.var", palette = c("#005580", "#b300b3", "#b33c00"),
             repel = TRUE, 
             title = "Cercle des corrélations sur le plan (1,5)",
             axes = c(1, 5),
             #geom = c("point", "text"), 
             legend = "bottom")
```

## Qualitative variables
```{r}
fviz_mfa_var(new_mfa, "quali.var",
             repel = TRUE,
             axes = c(1, 2),
             col.var = "contrib",
             select.var = list(contrib = 15),
             #geom = c("point", "text"), 
             legend = "bottom")
```

## Seperate analysis

### Acceptability :

```{r}
fviz_pca_biplot(new_mfa$separate.analyses$Acceptability_quant,
                label = "var",
                repel = TRUE,
                axes = c(1,2),
                habillage = vaccine, # color by groups 
                palette = c("#00AFBB", "#E7B800", "#FC4E07"),
                col.var = "#ff66ff",
                #select.ind = list(contrib = 50),
                title = "PCA biplot - Group Acceptability"
                #col.var = "#b300b3"
                )
```


```{r}
fviz_mca_biplot(new_mfa$separate.analyses$Acceptability_quali,
                label = "var",
                repel = TRUE,
                axes = c(1, 2),
                habillage = vaccine, # color by groups 
                palette = c("#00AFBB", "#E7B800", "#FC4E07"),
                col.var = "darkred",
                #select.ind = list(contrib = 250),
                select.var = list(contrib = 15),
                title = "MCA biplot - Group Acceptability"
                #col.var = "#b300b3"
                )
```

### Accessibility 

```{r quali}
fviz_mca_biplot(new_mfa$separate.analyses$Accessibility_quali,
                label = "var",
                repel = TRUE,
                habillage = vaccine, # color by groups 
                palette = c("#00AFBB", "#E7B800", "#FC4E07"),
                col.var = "darkred",
                select.ind = list(contrib = 1000),
                select.var = list(contrib = 25),
                title = "MCA biplot - Group Accessibility"
                #col.var = "#b300b3"
                )
```

### Affordability
```{r}
fviz_pca_biplot(new_mfa$separate.analyses$Affordability_quanti,
                label = "var",
                repel = TRUE,
                axes = c(1, 2),
                habillage = vaccine, # color by groups 
                palette = c("#00AFBB", "#E7B800", "#FC4E07"),
                col.var = "#ff66ff",
                title = "PCA biplot - Group Affordability"
                #col.var = "#b300b3"
                )
```


```{r}
fviz_mca_biplot(new_mfa$separate.analyses$Affordability_quali,
                label = "var",
                repel = TRUE,
                axes = c(1, 2),
                habillage = vaccine, # color by groups 
                palette = c("#00AFBB", "#E7B800", "#FC4E07"),
                col.var = "darkred",
                select.ind = list(contrib = 1000),
                select.var = list(contrib = 25),
                title = "MCA biplot - Group Affordability"
                #col.var = "#b300b3"
                )
```

```{r}
fviz_mca_biplot(new_mfa$separate.analyses$Availability,
                label = "var",
                repel = TRUE,
                axes = c(1, 2),
                habillage = vaccine, # color by groups 
                palette = c("#00AFBB", "#E7B800", "#FC4E07"),
                col.var = "darkred",
                select.ind = list(contrib = 3000),
                select.var = list(contrib = 20),
                title = "MCA biplot - Group Availability"
                #col.var = "#b300b3"
                )
```


```{r}
fviz_mca_biplot(new_mfa$separate.analyses$Awareness,
                label = "var",
                repel = TRUE,
                axes = c(1, 2),
                habillage = vaccine, # color by groups 
                palette = c("#00AFBB", "#E7B800", "#FC4E07"),
                col.var = "darkred",
                #select.ind = list(contrib = 1000),
                select.var = list(contrib = 25),
                title = "MCA biplot - Group Awareness"
                #col.var = "#b300b3"
                )
```

# Principal Component Regression

```{r}
# we use the first 7 PC for the regression
mfa_rcp <-
  MFA(
    newDF,
    group = c(1, 30, 12, 11, 3, 17, 7, 37),
    type = c("c", "n", "n", "n", "c", "n", "c", "n"),
    name.group = c(
      "Accessibility_quanti",
      "Accessibility_quali",
      "Availability",
      "Awareness",
      "Affordability_quanti",
      "Affordability_quali",
      "Acceptability_quanti",
      "Acceptability_quali"
    ),
    ncp = 7,
    graph = FALSE
  )
```


```{r rcp-model}
Accessibility_quant_1 = as.matrix(mfa_rcp$separate.analyses$Accessibility_quanti$ind$coord[, 1])
Accessibility_quali_1 = as.matrix(mfa_rcp$separate.analyses$Accessibility_quali$ind$coord[, 1])
Acceptability_quant_1 = as.matrix(mfa_rcp$separate.analyses$Acceptability_quant$ind$coord[, 1])
Acceptability_quali_1 = as.matrix(mfa_rcp$separate.analyses$Acceptability_quali$ind$coord[, 1])
Affordability_quant_1 = as.matrix(mfa_rcp$separate.analyses$Affordability_quanti$ind$coord[, 1])
Affordability_quali_1 = as.matrix(mfa_rcp$separate.analyses$Affordability_quali$ind$coord[, 1])
Availability_1 = as.matrix(mfa_rcp$separate.analyses$Availability$ind$coord[, 1])
Awareness_1 = as.matrix(mfa_rcp$separate.analyses$Awareness$ind$coord[, 1])

Y_cp = ifelse(std_data_all$vaccine == TRUE, 1, 0)
df_rcp <-
  data.frame(
    Y_cp,
    Accessibility_quant_1,
    Accessibility_quali_1,
    Affordability_quant_1,
    Affordability_quali_1,
    Acceptability_quant_1,
    Acceptability_quali_1,
    Availability_1,
    Awareness_1,
    cluster = std_data_all$grappe
  )
system.time({
  RCP_model <- glmer(Y_cp ~ Accessibility_quant_1 + Accessibility_quali_1 + Affordability_quant_1 + Affordability_quali_1 + Acceptability_quant_1 + Acceptability_quali_1 + Availability_1 + Awareness_1 + (1 | cluster), 
                       data = df_rcp, 
                       family = binomial(link = "logit"),
                       control = glmerControl(optimizer = "bobyqa",
                                              optCtrl = list(maxfun = 1e4)))
})
summary(RCP_model)
```
```{r}
# confidence intervals
round(confint(RCP_model, method = "Wald"), 4)[-1,]
```


# LASSO

```{r}
data_model = dplyr::select(std_data_all, -hhid, -owner_man, -owner_woman, -code_anim, -moy_transp_pr_freq)
data_model$cluster = as.factor(std_data_all$grappe)
data_model = dplyr::select(data_model, vaccine, region_nom:service_feed_bank, cluster)
```


```{r}
system.time({
LASSO_model = glmmLasso(
    fix = Y ~ as.factor(region_nom) + as.factor(head_sexe) + as.factor(head_martial_status) + as.factor(head_religion) +
      as.factor(head_phone) + as.factor(head_internet) + as.factor(head_education) +
      as.factor(head_economic_activity) + as.factor(head_bank_account) +
      as.factor(head_credit) + as.factor(owner_head) +
      as.factor(owner_gender) + as.factor(owner_married) +
      as.factor(owner_school) + as.factor(owner_internet) +
      as.factor(owner_economic_activity) + as.factor(owner_credit) +
      owner_num + owner_number_sold + owner_number_purchased +
      owner_value_purchased + owner_dairy_revenue + owner_healthcare_expenditure +
      as.factor(owner_decision_sale) + as.factor(owner_milk_production) +
      as.factor(owner_meat_production) + as.factor(owner_sale_part_meat) +
      as.factor(owner_sale_part_milk) + as.factor(owner_purchase_feed) +
      as.factor(owner_watering_payment) + as.factor(owner_deworming) +
      as.factor(owner_healthcare) + as.factor(household_cultive_terre) +
      as.factor(household_tv) + as.factor(household_transp) +
      as.factor(household_radio) + as.factor(household_tel) +
      as.factor(household_reseau_elect) + as.factor(household_reseau_tel) +
      as.factor(household_internet) + as.factor(entreprise_nonag_pres) +
      household_val_achat + household_prix_revente + household_wealth_immobilier +
      as.factor(household_choc) + as.factor(choc_str_epargne) +
      as.factor(choc_str_aide_parents_amis) + as.factor(choc_str_aide_gouv) +
      as.factor(choc_str_aide_rel_ONG) + as.factor(choc_str_marier_enfants) +
      as.factor(choc_str_change_conso) + as.factor(choc_str_achat_aliments_moins_chers) +
      as.factor(choc_str_enfants_travailler) +
      as.factor(choc_str_migration) +
      as.factor(choc_str_obtention_credit) + as.factor(choc_str_vente_actifs_agricoles) +
      as.factor(choc_str_vente_biens_durables) + as.factor(choc_str_vente_stock_vivres) + as.factor(choc_str_vente_betail) +
      as.factor(choc_str_confiage_enfants) + as.factor(choc_str_activites_spirituelles) +
      as.factor(choc_str_culture_contre_saison) + as.factor(choc_str_autre_strategie) +
      as.factor(choc_str_aucune_strategie) + as.factor(household_percep_rev) +
      as.factor(household_percep_rev_voisins) + as.factor(household_percep_rev_capitale) +
      as.factor(household_percep_rev_cl) + household_percep_rev_mttmin +
      as.factor(household_diff_loyer) + as.factor(household_diff_ecl) +
      as.factor(household_diff_maladie) + as.factor(household_diff_transport) +
      as.factor(household_diff_sco) + distance_ville_pr + as.factor(voie_acces_pr) + as.factor(moy_transp_pr) +
      as.factor(transp_dispo) + as.factor(transp_moto) + as.factor(transp_voiture) + as.factor(transp_pirogue) +
      as.factor(reseau_elect) + as.factor(reseau_mobile) +
      as.factor(service_hospital) + as.factor(service_other_health) +
      as.factor(service_medical_clinic) + as.factor(service_bank) +
      as.factor(service_permanent_market) + as.factor(service_periodic_market) +
      as.factor(service_agricultural_center) + as.factor(service_feed_bank),
    rnd = list(cluster = ~1),
    data = data_model,
    family = binomial(link = "logit"),
    lambda = 100,
    switch.NR = FALSE,
    final.re = TRUE,
    control = list(steps = 30, print.iter = TRUE, print.iter.final = TRUE, maxIter = 30)
  )
})  
  # Enregistrer le modèle avant d'executer le prochain
#save.image("~/Vaccination determinants (gitlab)/codes/models/model.RData")
```

```{r}
# this will probably take 7 hours at least
# On génère une grille de plusieurs valeurs de lambda possibles (on inclut pas 0 car cela signifie pas de pénalisation, ce n'est pas le but)
# On évalue le modèle sur chaque valeur de lambda et on sélectionnera le modèle qui minimise le BIC
#lambda_grid <- seq(0.1, 10, by = 0.5)
lambda_grid <- seq(10, 4.1, by = -0.5)

for (j in lambda_grid) {

  cat("Evaluating for lambda =", j, "\n")
  
  assign(paste0("LASSO_model_", j), glmmLasso(
    fix = Y ~ as.factor(region_nom) + as.factor(head_sexe) + as.factor(head_martial_status) + as.factor(head_religion) +
      as.factor(head_phone) + as.factor(head_internet) + as.factor(head_education) +
      as.factor(head_economic_activity) + as.factor(head_bank_account) +
      as.factor(head_credit) + as.factor(owner_head) +
      as.factor(owner_gender) + as.factor(owner_married) +
      as.factor(owner_school) + as.factor(owner_internet) +
      as.factor(owner_economic_activity) + as.factor(owner_credit) +
      owner_num + owner_number_sold + owner_number_purchased +
      owner_value_purchased + owner_dairy_revenue + owner_healthcare_expenditure +
      as.factor(owner_decision_sale) + as.factor(owner_milk_production) +
      as.factor(owner_meat_production) + as.factor(owner_sale_part_meat) +
      as.factor(owner_sale_part_milk) + as.factor(owner_purchase_feed) +
      as.factor(owner_watering_payment) + as.factor(owner_deworming) +
      as.factor(owner_healthcare) + as.factor(household_cultive_terre) +
      as.factor(household_tv) + as.factor(household_transp) +
      as.factor(household_radio) + as.factor(household_tel) +
      as.factor(household_reseau_elect) + as.factor(household_reseau_tel) +
      as.factor(household_internet) + as.factor(entreprise_nonag_pres) +
      household_val_achat + household_prix_revente + household_wealth_immobilier +
      as.factor(household_choc) + as.factor(choc_str_epargne) +
      as.factor(choc_str_aide_parents_amis) + as.factor(choc_str_aide_gouv) +
      as.factor(choc_str_aide_rel_ONG) + as.factor(choc_str_marier_enfants) +
      as.factor(choc_str_change_conso) + as.factor(choc_str_achat_aliments_moins_chers) +
      as.factor(choc_str_enfants_travailler) +
      as.factor(choc_str_migration) +
      as.factor(choc_str_obtention_credit) + as.factor(choc_str_vente_actifs_agricoles) +
      as.factor(choc_str_vente_biens_durables) + as.factor(choc_str_vente_stock_vivres) + as.factor(choc_str_vente_betail) +
      as.factor(choc_str_confiage_enfants) + as.factor(choc_str_activites_spirituelles) +
      as.factor(choc_str_culture_contre_saison) + as.factor(choc_str_autre_strategie) +
      as.factor(choc_str_aucune_strategie) + as.factor(household_percep_rev) +
      as.factor(household_percep_rev_voisins) + as.factor(household_percep_rev_capitale) +
      as.factor(household_percep_rev_cl) + household_percep_rev_mttmin +
      as.factor(household_diff_loyer) + as.factor(household_diff_ecl) +
      as.factor(household_diff_maladie) + as.factor(household_diff_transport) +
      as.factor(household_diff_sco) + distance_ville_pr + as.factor(voie_acces_pr) + as.factor(moy_transp_pr) +
      as.factor(transp_dispo) + as.factor(transp_moto) + as.factor(transp_voiture) + as.factor(transp_pirogue) +
      as.factor(reseau_elect) + as.factor(reseau_mobile) +
      as.factor(service_hospital) + as.factor(service_other_health) +
      as.factor(service_medical_clinic) + as.factor(service_bank) +
      as.factor(service_permanent_market) + as.factor(service_periodic_market) +
      as.factor(service_agricultural_center) + as.factor(service_feed_bank),
    rnd = list(cluster = ~1),
    data = data_model,
    family = binomial(link = "logit"),
    lambda = j,
    switch.NR = FALSE,
    final.re = TRUE,
    control = list(steps = 25, print.iter = TRUE, print.iter.final = TRUE, maxIter = 40)
  ))
  
  # Enregistrer le modèle avant d'executer le prochain
  save.image("~/Vaccination determinants (gitlab)/codes/models/model.RData")
}
```















```{r eval=FALSE}
# code is not working i don't know why i've tried everything to make it work..
library(cv.glmmLasso)
data_model = as.data.frame(data_model)
system.time({
  LASSO_model_cv = cv.glmmLasso(
    dat = data_model,  # The full dataset containing y (response var), X (fixed effects), Z (RE)
    fix = vaccine ~ as.factor(region_nom) + as.factor(head_sexe) + as.factor(head_martial_status) + 
      as.factor(head_religion) +  as.factor(head_phone) + 
      as.factor(head_internet) + as.factor(head_education) + 
      as.factor(head_economic_activity) + as.factor(head_bank_account) +
      as.factor(head_credit) + owner_num + owner_number_sold + owner_number_purchased +
      owner_value_purchased + owner_dairy_revenue + owner_healthcare_expenditure + as.factor(owner_head) + 
      as.factor(owner_gender) + as.factor(owner_married) +
      as.factor(owner_school) + as.factor(owner_internet) +
      as.factor(owner_economic_activity) + as.factor(owner_credit) +
      as.factor(owner_decision_sale) + as.factor(owner_milk_production) +
      as.factor(owner_meat_production) + as.factor(owner_sale_part_meat) +
      as.factor(owner_sale_part_milk) + as.factor(owner_purchase_feed) +
      as.factor(owner_watering_payment) + as.factor(owner_deworming) +
      as.factor(owner_healthcare) + as.factor(household_cultive_terre) +
      as.factor(household_tv) + as.factor(household_transp) +
      as.factor(household_radio) + as.factor(household_tel) +
      as.factor(household_reseau_elect) + as.factor(household_reseau_tel) +
      as.factor(household_internet) + as.factor(entreprise_nonag_pres) +
      household_val_achat + household_prix_revente + household_wealth_immobilier +
      as.factor(household_choc) + as.factor(choc_str_epargne) +
      as.factor(choc_str_aide_parents_amis) + as.factor(choc_str_aide_gouv) +
      as.factor(choc_str_aide_rel_ONG) + as.factor(choc_str_marier_enfants) +
      as.factor(choc_str_change_conso) + as.factor(choc_str_achat_aliments_moins_chers) +
      as.factor(choc_str_enfants_travailler) + as.factor(choc_str_migration) +
      as.factor(choc_str_obtention_credit) + as.factor(choc_str_vente_actifs_agricoles) +
      as.factor(choc_str_vente_biens_durables) + as.factor(choc_str_vente_stock_vivres) + 
      as.factor(choc_str_vente_betail) + as.factor(choc_str_confiage_enfants) + 
      as.factor(choc_str_activites_spirituelles) + as.factor(household_percep_rev) +
      as.factor(household_percep_rev_voisins) + as.factor(household_percep_rev_capitale) +
      as.factor(household_percep_rev_cl) + household_percep_rev_mttmin +
      as.factor(household_diff_loyer) + as.factor(household_diff_ecl) +
      as.factor(household_diff_maladie) + as.factor(household_diff_transport) +
      as.factor(household_diff_sco) + distance_ville_pr +
      as.factor(transp_dispo) + as.factor(transp_moto) + as.factor(transp_voiture) + 
      as.factor(transp_pirogue) + as.factor(moy_transp_pr) +
      as.factor(reseau_elect) + as.factor(reseau_mobile) +
      as.factor(service_hospital) + as.factor(service_other_health) +
      as.factor(service_medical_clinic) + as.factor(service_bank) +
      as.factor(service_permanent_market) + as.factor(service_periodic_market) +
      as.factor(service_agricultural_center) + as.factor(service_feed_bank),
    rnd = list(cluster = ~1),  # Random effects formula for the cluster
    family = binomial(link = "logit"),  # Family of the model
    kfold = 3,  # Number of folds for cross-validation
    nlambdas = 5,  # Number of lambda values to test
    lambda.min.ratio = 1e-04,  # Minimum ratio of lambda
    lambda.final = "lambda.1se"  # Choose the final lambda
  )
})
```



