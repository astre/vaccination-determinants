# ce sript contient les codes correspondants aux cartes présentes dans la section "données et méthodes" de mon mémoire
# site web pour télécharger les shapesfiles : https://gadm.org/download_country.html
# source du code : https://r-graph-gallery.com/175-choropleth-map-cartography-pkg.html
if(!require(pacman)){
  install.packages("pacman")
}
pacman::p_load("here",
               "sf",
               "dplyr",
               "tidyr",
               "ggplot2",
               "mapsf",
               "gridExtra",
               "tmap"
)

# chemin vers les données
here("codes/map_data/")
load("~/Vaccination determinants (gitlab)/codes/code_maps/map_data/data.RData")
ner_shapefile <- st_read(dsn = "codes/code_maps/map_data/NER/gadm41_NER_1.shp")
bfa_shapefile <- st_read(dsn = "codes/code_maps/map_data/BFA/gadm41_BFA_1.shp")
ben_shapefile <- st_read(dsn = "codes/code_maps/map_data/BEN/geoBoundaries-BEN-ADM1.shp")
mli_shapefile <- st_read(dsn = "codes/code_maps/map_data/MLI/geoBoundaries-MLI-ADM1.shp")
civ_shapefile <- st_read(dsn = "codes/code_maps/map_data/CIV/gadm41_CIV_2.shp")
sen_shapefile <- st_read(dsn = "codes/code_maps/map_data/SEN/geoBoundaries-SEN-ADM1.shp")
tgo_shapefile <- st_read(dsn = "codes/code_maps/map_data/TGO/geoBoundaries-TGO-ADM1.shp")


# Fonction pour calculer les proportions de chèvres et moutons
calc_proportion <- function(data, region_code_col, has_goat_col, has_sheep_col) {
  data %>%
    group_by(!!sym(region_code_col)) %>%
    summarise(
      total_households = n(),
      households_with_goats = sum(!!sym(has_goat_col) == TRUE, na.rm = TRUE),
      households_with_sheep = sum(!!sym(has_sheep_col) == TRUE, na.rm = TRUE),
      proportion_with_goats = households_with_goats / total_households,
      proportion_with_sheep = households_with_sheep / total_households
    )
}

# Calcul des proportions pour chaque pays
prop_ner <- calc_proportion(data_ner, "region_nom", "has_goat", "has_sheep")
prop_ben <- calc_proportion(data_ben, "region_nom", "has_goat", "has_sheep")
prop_bfa <- calc_proportion(data_bfa, "region_nom", "has_goat", "has_sheep")
prop_mli <- calc_proportion(data_mli, "region_nom", "has_goat", "has_sheep")
prop_civ <- calc_proportion(data_civ, "region_nom", "has_goat", "has_sheep")
prop_sen <- calc_proportion(data_sen, "region_nom", "has_goat", "has_sheep")
prop_tgo <- calc_proportion(data_tgo, "region_nom", "has_goat", "has_sheep")


# renommer les regions pour que ce soit conforme aux noms des régions dans le shapefile
prop_ner$region_nom = c("Agadez", "Diffa", "Dosso", "Maradi", "Niamey", "Tahoua", "Tillabéry", "Zinder")
prop_bfa$region_nom = bfa_shapefile$NAME_1

prop_ben$region_nom = ben_shapefile$shapeName
# Il y a une région qui est pas nommé pareil : "Koulikoro" en "Koulikouro"
prop_mli <- prop_mli %>%
  mutate(region_nom = if_else(region_nom == "Koulikoro", "Koulikouro", region_nom))
prop_civ$region_nom = c("Agnéby-Tiassa", "Abidjan", "Bafing", "Bagoué", "Bélier",
                        "Béré", "Bounkani", "Cavally", "Folon", "Gbeke",
                        "Gbôkle", "Gôh", "Gontougo", "Grands Ponts",
                        "Guémon", "Hambol", "Haut-Sassandra", "Iffou", "Indénié-Djuablin",
                        "Kabadougou", "La Mé", "Lôh-Djiboua", "Marahoué", "Moronou",
                        "N'zi", "Nawa", "Poro", "San-Pédro", "Sud Comoé", "Tchologo",        
                        "Tonkpi", "Worodougou", "Yamoussoukro")

prop_sen$region_nom = sen_shapefile$shapeName
prop_tgo$region_nom = c("Centrale Region", "Kara Region", "Lomme commune", "Maritime Region", "Plateaux Region", "Savanes Region")

# Fusionner les données avec les shapefiles
ner_shapefile <- ner_shapefile %>% 
  left_join(prop_ner, by = c("NAME_1" = "region_nom")) %>% select(-ends_with(".x"))

ben_shapefile <- ben_shapefile %>% 
  left_join(prop_ben, by = c("shapeName" = "region_nom"))

bfa_shapefile <- bfa_shapefile %>% 
  left_join(prop_bfa, by = c("NAME_1" = "region_nom")) %>% select(-ends_with(".x"))

mli_shapefile <- mli_shapefile %>%
  left_join(prop_mli, by = c("shapeName" = "region_nom")) %>% select(-ends_with(".x"))

civ_shapefile <- civ_shapefile %>% 
  left_join(prop_civ, by = c("NAME_2" = "region_nom")) %>% select(-ends_with(".x"))

sen_shapefile <- sen_shapefile %>% 
  left_join(prop_sen, by = c("shapeName" = "region_nom")) %>% select(-ends_with(".x"))

tgo_shapefile <- tgo_shapefile %>% 
  left_join(prop_tgo, by = c("shapeName" = "region_nom")) %>% select(-ends_with(".x"))



##### map niger améliorée #####
tmap_mode("plot")

# Carte pour le total des ménages interviewés au Niger en vert
map_ner_households <- tm_shape(ner_shapefile) +
  tm_compass(position = c("left","top")) +
  tm_fill("total_households",
          palette = "Dark Mint",
          title = "(a)",
          breaks = c(0, 100, 200, 300, 400, 500, 600, 700),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) 
  #tm_add_legend("title", labels = "(a)", col = "white", size = 0.0001)
map_ner_households
# colorspace::hcl_palettes(plot = TRUE) # pour visualiser les palettes de couleur

tmap_save(map_ner_households, "total_households_niger.pdf",
            width = 15, height = 10)

# Carte pour la proportion des ménages avec chèvres au Niger
map_ner_goats <- tm_shape(ner_shapefile) +
  tm_fill("proportion_with_goats",
          palette = "BuPu",
          title = "(b)",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  #tm_add_legend("title", labels = "(b)", col = "white", size = 0.0001)+
  tm_compass(position = c("left","top")) 
map_ner_goats
  #+
  # tm_layout(main.title = "Proportion de ménages avec des chèvres au Niger",
  #           main.title.size = 1.5,
  #           main.title.position = "center")
map_ner_goats
tmap_save(map_ner_goats, "proportion_goats_niger.pdf",
          width = 15, height = 10)

# Carte pour la proportion des ménages avec moutons au Niger
map_ner_sheep <- tm_shape(ner_shapefile) +
  tm_fill("proportion_with_sheep",
          title = "(c)",
          palette = "Burg",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  #tm_add_legend("title", labels = "(b)", col = "white", size = 0.0001)+
  tm_compass(position = c("left","top")) 
map_ner_sheep
tmap_save(map_ner_sheep, "proportion_sheep_niger.pdf",
 width = 15, height = 10)

map = tmap_arrange(map_ner_households, map_ner_goats, map_ner_sheep, ncol = 3)
map

##### map benin améliorée ######
tmap_mode("plot")

# Carte pour le total des ménages interviewés au benin
map_ben_households <- tm_shape(ben_shapefile) +
  tm_fill("total_households",
          palette = "Dark Mint",
          title = "(a)",
          breaks = c(0, 100, 200, 300),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_compass(position = c("left","top")) +
  tm_layout(legend.outside = TRUE,
            legend.outside.position = "bottom",
            legend.stack = "horizontal",
            legend.text.size = 0.55,
            legend.title.size = 2)

map_ben_households
# colorspace::hcl_palettes(plot = TRUE) # pour visualiser les palettes de couleur

tmap_save(map_ben_households, "total_households_benin.pdf",
          width = 5, height = 10)

# Carte pour la proportion des ménages avec chèvres au benin
map_ben_goats <- tm_shape(ben_shapefile) +
  tm_fill("proportion_with_goats",
          palette = "BuPu",
          title = "(b)",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("left","top")) 
map_ben_goats

tmap_save(map_ben_goats, "proportion_goats_benin.pdf",
          width = 5, height = 10)

# Carte pour la proportion des ménages avec moutons au benin
map_ben_sheep <- tm_shape(ben_shapefile) +
  tm_fill("proportion_with_sheep",
          title = "(c)",
          palette = "Burg",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("left","top")) 
map_ben_sheep
tmap_save(map_ben_sheep, "proportion_sheep_benin.pdf",
          width = 5, height = 10)


##### map bfa améliorée ######
tmap_mode("plot")

# Carte pour le total des ménages interviewés au benin
map_bfa_households <- tm_shape(bfa_shapefile) +
  tm_fill("total_households",
          palette = "Dark Mint",
          title = "(a)",
          breaks = c(0, 100, 200, 300),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_compass(position = c("left","top")) +
  tm_layout(legend.outside = TRUE,
            legend.outside.position = "bottom",
            legend.stack = "horizontal",
            legend.text.size = 0.8,
            legend.title.size = 2)

map_bfa_households
# colorspace::hcl_palettes(plot = TRUE) # pour visualiser les palettes de couleur

tmap_save(map_bfa_households, "total_households_bfa.pdf",
          width = 8, height = 10)

# Carte pour la proportion des ménages avec chèvres au bfa
map_bfa_goats <- tm_shape(bfa_shapefile) +
  tm_fill("proportion_with_goats",
          palette = "BuPu",
          title = "(b)",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("left","top")) 
map_bfa_goats

tmap_save(map_bfa_goats, "proportion_goats_bfa.pdf",
          width = 8, height = 10)

# Carte pour la proportion des ménages avec moutons au bfa
map_bfa_sheep <- tm_shape(bfa_shapefile) +
  tm_fill("proportion_with_sheep",
          title = "(c)",
          palette = "Burg",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("left","top")) 
map_bfa_sheep
tmap_save(map_bfa_sheep, "proportion_sheep_bfa.pdf",
          width = 8, height = 10)

##### map togo améliorée ######
tmap_mode("plot")

# Carte pour le total des ménages interviewés au benin
map_tgo_households <- tm_shape(tgo_shapefile) +
  tm_fill("total_households",
          palette = "Dark Mint",
          title = "(a)",
          breaks = c(0, 100, 200, 300),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_compass(position = c("right","top")) +
  tm_layout(legend.outside = TRUE,
            legend.outside.position = "bottom",
            legend.stack = "horizontal",
            legend.text.size = 0.55,
            legend.title.size = 2)

map_tgo_households
# colorspace::hcl_palettes(plot = TRUE) # pour visualiser les palettes de couleur

tmap_save(map_tgo_households, "total_households_tgo.pdf",
          width = 5, height = 10)

# Carte pour la proportion des ménages avec chèvres au tgo
map_tgo_goats <- tm_shape(tgo_shapefile) +
  tm_fill("proportion_with_goats",
          palette = "BuPu",
          title = "(b)",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("right","top")) 
map_tgo_goats

tmap_save(map_tgo_goats, "proportion_goats_tgo.pdf",
          width = 5, height = 10)

# Carte pour la proportion des ménages avec moutons au tgo
map_tgo_sheep <- tm_shape(tgo_shapefile) +
  tm_fill("proportion_with_sheep",
          title = "(c)",
          palette = "Burg",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("right","top")) 
map_tgo_sheep
tmap_save(map_tgo_sheep, "proportion_sheep_tgo.pdf",
          width = 5, height = 10)



##### map senegal améliorée ######
tmap_mode("plot")

# Carte pour le total des ménages interviewés au sen
map_sen_households <- tm_shape(sen_shapefile) +
  tm_fill("total_households",
          palette = "Dark Mint",
          title = "(a)",
          breaks = c(0, 100, 200, 300),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_compass(position = c("left","top")) +
  tm_layout(legend.outside = TRUE,
            legend.outside.position = "bottom",
            legend.stack = "horizontal",
            legend.text.size = 0.55,
            legend.title.size = 2)

map_sen_households
# colorspace::hcl_palettes(plot = TRUE) # pour visualiser les palettes de couleur

tmap_save(map_sen_households, "total_households_sen.pdf",
          width = 7, height = 10)

# Carte pour la proportion des ménages avec chèvres au sen
map_sen_goats <- tm_shape(sen_shapefile) +
  tm_fill("proportion_with_goats",
          palette = "BuPu",
          title = "(b)",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("left","top")) 
map_sen_goats

tmap_save(map_sen_goats, "proportion_goats_sen.pdf",
          width = 7, height = 10)

# Carte pour la proportion des ménages avec moutons au sen
map_sen_sheep <- tm_shape(sen_shapefile) +
  tm_fill("proportion_with_sheep",
          title = "(c)",
          palette = "Burg",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("left","top")) 
map_sen_sheep
tmap_save(map_sen_sheep, "proportion_sheep_sen.pdf",
          width = 7, height = 10)

##### map mali améliorée ######
tmap_mode("plot")

# Carte pour le total des ménages interviewés au mali
map_mli_households <- tm_shape(mli_shapefile) +
  tm_fill("total_households",
          palette = "Dark Mint",
          title = "(a)",
          breaks = c(0, 100, 200, 300),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_compass(position = c("left","top")) +
  tm_layout(legend.outside = TRUE,
            legend.outside.position = "bottom",
            legend.stack = "horizontal",
            legend.text.size = 0.55,
            legend.title.size = 2)

map_mli_households
# colorspace::hcl_palettes(plot = TRUE) # pour visualiser les palettes de couleur

tmap_save(map_mli_households, "total_households_mali.pdf",
          width = 7, height = 10)

# Carte pour la proportion des ménages avec chèvres au mali
map_mli_goats <- tm_shape(mli_shapefile) +
  tm_fill("proportion_with_goats",
          palette = "BuPu",
          title = "(b)",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("left","top")) 
map_mli_goats

tmap_save(map_mli_goats, "proportion_goats_mali.pdf",
          width = 7, height = 10)

# Carte pour la proportion des ménages avec moutons au mali
map_mli_sheep <- tm_shape(mli_shapefile) +
  tm_fill("proportion_with_sheep",
          title = "(c)",
          palette = "Burg",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("left","top")) 
map_mli_sheep
tmap_save(map_mli_sheep, "proportion_sheep_mali.pdf",
          width = 7, height = 10)

##### map civ améliorée ######
tmap_mode("plot")

# Carte pour le total des ménages interviewés au civ
map_civ_households <- tm_shape(civ_shapefile) +
  tm_fill("total_households",
          palette = "Dark Mint",
          title = "(a)",
          breaks = c(0, 100, 200, 300),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_compass(position = c("left","top")) +
  tm_layout(legend.outside = TRUE,
            legend.outside.position = "bottom",
            legend.stack = "horizontal",
            legend.text.size = 0.55,
            legend.title.size = 2)

map_civ_households
# colorspace::hcl_palettes(plot = TRUE) # pour visualiser les palettes de couleur

tmap_save(map_civ_households, "total_households_civ.pdf",
          width = 7, height = 10)

# Carte pour la proportion des ménages avec chèvres au civ
map_civ_goats <- tm_shape(civ_shapefile) +
  tm_fill("proportion_with_goats",
          palette = "BuPu",
          title = "(b)",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("left","top")) 
map_civ_goats

tmap_save(map_civ_goats, "proportion_goats_civ.pdf",
          width = 7, height = 10)

# Carte pour la proportion des ménages avec moutons au civ
map_civ_sheep <- tm_shape(civ_shapefile) +
  tm_fill("proportion_with_sheep",
          title = "(c)",
          palette = "Burg",
          breaks = seq(0, 1, by = 0.1),
          style = "cont",
          legend.is.portrait = FALSE) +
  tm_layout(legend.outside.position = "bottom",
            legend.outside.size = 0.55,
            legend.outside = TRUE,
            legend.title.size = 2) +
  tm_compass(position = c("left","top")) 
map_civ_sheep
tmap_save(map_civ_sheep, "proportion_sheep_civ.pdf",
          width = 7, height = 10)


####### carte de l'Afrique avec les ménages interviewé et ceux ayant déclaré avoir vacciné #######
# Charger le shapefile du continent africain
setwd("D:/Mes Donnees/Stage/Rapport stage/data files")
africa_shapefile <- st_read(dsn = "codes/code_maps/map_data/Africa/Africa_Boundaries.shp")

west_africa_shapefile <- rbind(ben_shapefile, tgo_shapefile)
west_africa_shapefile = rbind(west_africa_shapefile, sen_shapefile)
west_africa_shapefile = rbind(west_africa_shapefile, mli_shapefile)
# pas les mêmes noms de colonnes => uniformiser tout cela mais normalement c'est ok
west_africa_shapefile = rbind(west_africa_shapefile, ner_shapefile)
west_africa_shapefile = rbind(west_africa_shapefile, bfa_shapefile)
west_africa_shapefile = rbind(west_africa_shapefile, civ_shapefile)

# Corriger les géométries des shapefiles
# africa_shapefile <- st_make_valid(africa_shapefile)
# west_africa_shapefile <- st_make_valid(west_africa_shapefile)
# 
# # Vérifier les noms des colonnes
# print(colnames(africa_shapefile))
# print(colnames(west_africa_shapefile))

# Jointure entre africa_shapefile et west_africa_shapefile
africa = as.data.frame(africa_shapefile)
west = as.data.frame(west_africa_shapefile)
joined_df <- left_join(west, 
                       africa,
                       by = c("shapeGroup" = "ISO"))


joined_df = st_as_sf(joined_df)
joined_df = joined_df %>% select(-geometry.x)
#st_geometry(joined_df) <- st_geometry(west_africa_shapefile)

### Étape 3 : Créer la carte combinée

tmap_mode("plot")

map_africa <- tm_shape(joined_df) +
  tm_borders() +
  tm_polygons("total_households", 
              title = "Total ménages", 
              palette = "Blues", 
              style = "cont") +
  tm_compass(position = c("left", "top")) +
  tm_scalebar(position = c("center", "bottom")) +
  tm_layout(main.title = "Nombre total de ménages interviewés en Afrique de l'Ouest",
            main.title.size = 1.5,
            main.title.position = "center")

# Enregistrer la carte
tmap_save(map_africa, "africa_households_map2.pdf", width = 15, height = 10)


###### carte afrique avec pays de la zone d'étude #######

# Charger le shapefile du continent africain
africa_shapefile <- st_read("Africa/Africa_Boundaries.shp") %>% st_make_valid()

# Filtrer les pays de l'Afrique de l'Ouest
west_africa_countries <- c("Niger", "Senegal", "Burkina Faso", "Togo", "Côte d'Ivoire", "Mali", "Benin")
west_africa_shapefile <- africa_shapefile %>%
  filter(NAME_0 %in% west_africa_countries)

# Créer une variable pour colorier les pays sélectionnés
africa_shapefile$selected <- ifelse(africa_shapefile$NAME_0 %in% west_africa_countries, "Study Zone", "Other")

# Tracer la carte
map_west_africa <- tm_shape(africa_shapefile) +
  tm_polygons("selected",
              palette = c("Study Zone" = "#006666", "Other" = "#d6f5f5"),
              title = "") +
  tm_layout(main.title = "",
            main.title.size = 1.5,
            main.title.position = "center",
            legend.position = c("left", "bottom"),
            legend.outside = TRUE,
            legend.outside.position = "bottom",
            legend.stack = "horizontal") +
  tm_compass(position = c("right","top")) 

map_west_africa

#### map of clusters using GPS coordinate #####
setwd("D:/Mes Donnees/Vaccination determinants (gitlab)/codes/code_maps/map_data/GPS coordinate")

##### niger GPS #####

grappes_ner = read.csv("grappe_gps_ner2018.csv")
# Rename columns for easier access
colnames(grappes_ner) <- c("cluster", "wave", "latitude", "longitude", "accuracy", "altitude", "timestamp")

# Convert Latitude and Longitude to numeric
grappes_ner$latitude <- as.numeric(grappes_ner$latitude)
grappes_ner$longitude <- as.numeric(grappes_ner$longitude)
grappes_ner = grappes_ner %>% drop_na()

# Convert the data to a spatial object
data_sf <- st_as_sf(grappes_ner, coords = c("longitude", "latitude"), crs = 4326)  # WGS84

# Load the map of Niger
library(rnaturalearth)
niger_map <- ne_countries(scale = "medium", country = "Niger", returnclass = "sf")

ggplot(data = niger_map) +
  geom_sf(fill = "lightgrey") +
  geom_sf(data = data_sf, aes(color = cluster), size = 3, shape = 21) +  # Points for clusters
  labs(title = "Clusters in Niger", x = "Longitude", y = "Latitude", color = "Cluster") +
  theme_minimal()


# Tracer la carte
ggplot(data = niger_map) +
  geom_sf(fill = "lightblue") +  # Remplir la carte avec une couleur claire
  geom_sf(data = data_sf, color = "#003333", size = 3, shape = 21) +  # Utiliser une bulle bleue pour les clusters
  labs(title = "Clusters selected in Niger", x = "Longitude", y = "Latitude") +
  theme_minimal() +
  theme(legend.position = "none")  # Masquer la légende