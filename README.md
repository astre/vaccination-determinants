# Vaccination determinants

## Repository structure

Here is a diagram that shows how each directory and file to help you navigate through this repository :

``` bash
Repository Root
|
|-- codes/
| |-- code_maps
|   | -- map_data
|   | -- maps_script_sarah.R
|
| |-- data_analysis
|   | -- FAMD_sarah_matoub.Rmd
|   | -- MFA_HCPC_sarah_matoub.Rmd
|
| |--data_extraction
|   |-- data_extraction_<country>.R
|
| | -- models
|   | -- frequentist_models_sarah_matoub.Rmd
|   | -- bayesian_models_sarah_matoub.Rmd
|
|-- documents/
|  |-- conceptual_framework_sarah_matoub.pdf
|  |-- data_dictionnary_sarah_matoub.pdf
|  |-- diapositives_soutenance.pdf
|  |-- mémoire_stage_sarah_matoub.pdf
|
|-- extracted data/
| |-- data_<country>.csv
|
|-- original data/
| |-- EHCVM <Country>/
| |-- data/
| |-- documentation/
| |-- questionnaires/
|
|-- source/
| |-- functions_<country>.R
|
|-- .gitignore
|-- README.md
|-- Vaccination determinants.Rproj
```
The repository is organized into several directories and files, each serving a specific purpose within the project :

-   The `original data` directory contains 7 subdirectories, each corresponding to a different Francophone country, here is a detailed list of the datasets contained within these subdirectories, along with the specific websites from which the data was sourced :

    - [EHCVM Niger](https://doi.org/10.48529/ggam-ax39).
    - [EHCVM Benin](https://doi.org/10.48529/rn3k-z374).
    - [EHCVM Burkina Faso](https://doi.org/10.48529/wv88-j486).
    - [EHCVM Côte d'Ivoire](https://doi.org/10.48529/8wh3-bf40).
    - [EHCVM Mali](https://doi.org/10.48529/90e9-4e91).
    - [EHCVM Sénégal](https://microdata.worldbank.org/index.php/catalog/4297).
    - [EHCVM Togo](https://doi.org/10.48529/ww9z-d865).

Within each country-specific folder, there are 3 other folders : data, documentation, and questionnaires.

Only the essential csv files used in the code were uploaded to these folders to conserve space in the repository. In addition,data extraction for all countries will focus exclusively on the year 2018/2019.

-   The `codes` directory contains a collection of scripts for data extraction and database manipulations of LSMS data for specific Francophone countries :

**`data_extraction_niger.R`** for Niger, **`data_extraction_benin.R`** for Benin, **`data_extraction_bfa.R`** for Burkina Faso, **`data_extraction_civ.R`** for Côte d'Ivoire, **`data_extraction_mali.R`** for Mali, **`data_extraction_sen.R`** for Senegal, and **`data_extraction_togo.R`** for Togo.

-   The `source` directory is structured to include individual function files for each country, such as `functions_ben.R` for Benin, `functions_bfa.R` for Burkina Faso, `functions_civ.R` for Côte d'Ivoire, etc. Each of these files contains functions that have been used for data extraction processes for their respective countries.

- The `documents` directory includes `data_dictionary_thematic.xlsx`, which categorizes variables across different dimensions of vaccine access according to the conceptual framework provided in `conceptual_framework.pdf`.

-   The `extracted data` directory contains set of CSV files for each country, including:
`data_niger.csv` for Niger, along with corresponding data files for Benin, Burkina Faso, Côte d'Ivoire, Mali, Nigeria, Senegal, and Togo. These files contain data extracted as per the scripts in the codes directory.

-   Additionally, a `.gitignore` file is present to keep the repository clean by excluding files that should not be tracked by Git.

Before testing this code from the repository, please make sure that you are in the **`Vaccination determinants`** project directory :

``` bash
cd /path/to/Vaccination determinants
```

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

-   [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
-   [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```         
cd existing_repo
git remote add origin https://gitlab.cirad.fr/astre/vaccination-determinants.git
git branch -M master
git push -uf origin master
```

## Integrate with your tools

-   [ ] [Set up project integrations](https://gitlab.cirad.fr/astre/vaccination-determinants/-/settings/integrations)

## Collaborate with your team

-   [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
-   [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
-   [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
-   [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
-   [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

-   [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
-   [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
-   [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
-   [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
-   [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

------------------------------------------------------------------------

# Editing this README {#editing-this-readme}

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name

Choose a self-explaining name for your project.

## Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
